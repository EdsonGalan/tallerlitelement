import {LitElement, html, css} from 'lit-element';

export class TaskToDo extends LitElement{

  static get styles(){
    return css`
      .tasks-container{
        padding:5px;
      }
      div label{
        font-size:18px;
        margin-top: 2px;
        position: absolute;
      }
      div input{
        width: 20px;
        height: 20px;
      }
    `;
  }

  static get properties(){
    return {
      task: { type:String },
      created: { type:String },
      class_color: { type:String }
    };
  }

  constructor(){
    super();
  }

  render(){
    return html`
      <div class="tasks-container">
        <input type="checkbox" id="${this.task}" name="${this.task}">
        <label for="${this.task}">${this.task}</label>
      </div>
    `;
  }

}customElements.define('task-todo', TaskToDo);
