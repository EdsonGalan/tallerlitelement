import {LitElement, html, css} from 'lit-element';
// import '/src/todo-list.js';

export class ToDoList extends LitElement{

  static get styles() {
    return css`
      h1{
        text-align: center;
      }
      legend{
        font-size:18px;
      }
      input{
        width:98%;
        height:30px;
        font-size:16px;
        padding:3px 5px 3px 5px;
      }
      .btn{
        margin-top:5px;
        width:100%;
        height:30px;
        font-size:16px;
      }
      .hidden{
        display:none;
      }
    `;
  }

  static get properties() {
    return {
      title: { type:String },
      label: { type:String },
      placeholder: { type:String },
      taskname: { type:String },
      tasks: { type:Array },
      btn_archive: { type:String },
      count: { type:Number }
    };
  }

  constructor(){
    super();
    this.title = "My To do list";
    this.label = "New task";
    this.placeholder = "Type a task to do...";
    this.taskname = "";
    this.tasks = [];
    this.btn_archive = "hidden";
    this.count = 1;
  }

  _readTask(e){
    this.taskname = e.target.value;
  }

  _addTask(){
    let task = this.taskname;
    let created = "";
    let class_color = "";
    let tmp = new Date();
    created = tmp.getHours() + ":" + tmp.getMinutes() + ":" + tmp.getSeconds();
    this.tasks.push({
      "task":task,
      "created":created,
      "class_color":class_color
    });
    this.taskname = "";
    this.btn_archive = "show";
    this.count++;
    console.log(this.tasks);
  }

  _taskExists(){
    if( this.tasks.lenght ){
      this.btn_archive = "show";
    }else{
      this.btn_archive = "hidden";
    }
  }

  _archiveTask(){
    console.log(this.tasks);
  }

  render(){
    return html`
      <h1>${this.title}</h1>
      <fieldset>
        <legend>${this.label}</legend>
        <input type="text" name="task" @blur="${this._readTask}" placeholder="${this.placeholder}" value="${this.taskname}">
        <button type="button" class="btn" @click="${this._addTask}">Add task</button>
      </fieldset>
      <section>
        ${this.tasks.map(task => html`
          <task-todo task="${task.task}" created="${this.created}"
          class_color="${this.class_color}"></task-todo>
        `)}
      </section>
      <section class="${this.btn_archive}">
        <button type="button" class="btn" @click="${this._archiveTask}">Archive Done</button>
      </section>
    `;
  }
}customElements.define('todo-list', ToDoList);
