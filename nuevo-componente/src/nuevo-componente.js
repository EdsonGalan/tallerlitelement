import {html, css, LitElement} from 'lit-element';

export class Ejemplo1 extends LitElement{
  static get styles(){
    return css`
      :host{
        display:block;
        padding:25px;
        color:
      }
    `;
  }

  static get properties(){
    return{
      name:{type:String},
      counter:{type:Number},
    };
  }

  constructor(){
    super();
    this.name = 'Pedro';
    this.counter = 5;
  }

  __increment(){
    this.counter += 1;
  }

  render(){
    return html`
      <h2>Nombre: ${this.nombre}</h2>
      <h3>Contador: ${this.counter}</h3>
      <button @click="${this.__increment}">Incrementar</button>
    `;
  }
}customElements.define('nuevo-componente', Ejemplo1);
