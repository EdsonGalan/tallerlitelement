import { LitElement, html } from 'lit-element';

export class OtroComponente extends LitElement{
  render(){
    return html`
      <h2>Segundo componente</h2>
      <h3>Lit Element</h3>
    `;
  }
}customElements.define('otro-componente', OtroComponente);
