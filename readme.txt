Este es el que usaba antes
export NVM_DIR=~/.nvm
 source $(brew --prefix nvm)/nvm.sh

Este es el otro
export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# ..:: CONFIGURACIÓN ::..
# Intalar versión de Node 12.16.1 o 12.22.3 ( nvm install 12.22.3 o nvm install 12.16.1)
# Ver qué versión de Node tengo ( node -version )
# Ver las versiones de node: nvm -ls
# Cambiar de versión de Node ( nvm use v8.16.1 )
# Instalar Polymer: npm install -g polymer-cli
# Ver las versiones de node:  nvm -ls

# ..:: CREACIÓN DEL COMPONENTE ::..
# Dentro de la carpeta intro-lit -> npm init && npm install lit-element
# Iniciar el servidor: polymer serve
