import { html, css, LitElement } from 'lit-element';

export class Ejemplo1 extends LitElement {
	static get styles() {
		return css`
			:host {
				display: block;
				padding: 50px;
				color: var(--lit-element-color-var, #4f169f);
			}
			.nameClass {
				color: red;
			}
			.nameClass2 {
				color:blue;
				display: inline-block;
			}
		`;
	}

	static get properties() {
		return {
			name: { type: String },
			counter: { type: Number },
			nameColor: { type: String },
			arr: { type: Array },
			bool: { type: Boolean }
		};
	}

	constructor() {
		super();
		this.name = 'Pedro';
		this.counter = 5;
		this.nameColor = '';
		this.arr = [
			'Este',
			'es',
			'un',
			'arreglo',
		];
		this.bool = true;
	}

	__increment() {
		this.counter += 1;
		this.name = 'Abiel';
		this.nameColor = 'nameClass';
		// this.arr.push(2,3,4,5,6,7);
		this.arr = [2,3,4,5,6,7];
	}
	__increment2() {
		this.counter += 1;
		this.name = 'Pedro';
		this.nameColor = '';
		this.bool = !this.bool;
	}

	render(){
		return html`
			<h2 class="${this.nameColor}">Nombre: ${this.name}</h2><br>
			<h3>Contador: ${this.counter}</h3>
			<button @click="${this.__increment}">Incrementar</button>
			<button @click="${this.__increment2}">Incrementar2</button>
			<br><hr>

			<ul>
				${this.arr.map(i => html`<li>${i}</li>`)}
			</ul>

			<br><hr>

			${this.bool ? html`<p>Render some html if Bool is true</p>` : html`<p>Render other html if Bool is false</p>`}

		`;
	}
}

customElements.define('ejemplo-1', Ejemplo1);