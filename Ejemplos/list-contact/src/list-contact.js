import { html, LitElement } from 'lit-element';
import './e-contact.js';

export class ListContact extends LitElement {

	static get properties() {
		return {
			contacts: { type: Array },
			info: { type: String},
			info2: { type: String}
		};
	}

	constructor() {
		super();
		this.contacts = [
			{
				arrname: 'Juan Perez',
				arremail: 'juanito@gmail.com',
			},
			{
				arrname: 'Maria Benitez',
				arremail: 'MariBeni@gmail.com'
			},
			{
				arrname: 'Pedro Calvario',
				arremail: 'petercalva@gmail.com'
			}
		];
		this.info = '';
		this.info2 = '';
	}

	dataHandler(event) {
		this.info = event.detail.edad;
		this.info2 = event.detail.nombre;
		console.info(event.target);
	}

	render() {
		return html`
			<div>
				${this.contacts.map(contact => {
					return html`
					<ul>
						<li>
							<e-contact
								name="${contact.arrname}"
								email="${contact.arremail}"
								@my-event="${this.dataHandler}"
							></e-contact>
						</li>
					</ul>
					`;
				})}
			</div>
			${this.info}
			${this.info2}

		`;
	}
}

customElements.define('list-contact', ListContact)



// Hacer 1 componente que importe minimo 2 componentes
// La interaccion con los objetos tiene que ser de minimo 5 objetos
// La informacion dentro del objeto, tiene que contener minimo 3 campos
