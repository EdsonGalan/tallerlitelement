import { html, css, LitElement } from 'lit-element';

export class eContact extends LitElement {
  static get styles() {
    return css`
      div {
        border: 1px solid black;
        padding: 10px;
        border-radius: 5px;
        display: inline-block;
      }
      h1 {
        font-size: 1.2rem;
        font-weight: normal
      }
    `;
  }


  static get properties() {
    return {
      name: { type: String },
      email: { type: String },
      seeMore: { type: Boolean }
    }
  };

  constructor() {
    super();
    this.name = '';
    this.email = '';
    this.seeMore = false;
  }

  toggle() {
    this.seeMore = !this.seeMore;
  }

  dataHandler() {
    // let data = 'Informacion lanzada desde el evento';
    let data = {
      nombre: 'Abiel',
      edad: 25
    }
    this.dispatchEvent(new CustomEvent('my-event', {
      bubbles: true,
      composed: true,
      detail: data
    }));
  }

  render() {
    return html`
    <div>
      <h1>${this.name}</h1>
      <p><a href="#" @click="${this.toggle}">See More</a></p>

      ${this.seeMore ? html`Email: ${this.email}` : ''}

      <button @click="${this.dataHandler}">Boton</button>
    </div>
    `;
  }
}

customElements.define('e-contact', eContact);