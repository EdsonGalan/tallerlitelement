import {html, css, LitElement} from 'lit-element';

export class MyElement extends LitElement{
  static get style(){
    return css`
      :host{
        display:block;
        padding:25px;
        color:var(--litelements-openwc-text-color, #000);
      }
    `;
  }

  static get properties(){
    return{
      name:{type:String},
      count:{type:Number},
    }
  }

  constructor(){
    super();
    this.name = "World";
    this.count = 0;
  }

  render(){
    return html`
      <h1>Hello ${this.name}!</h1>
      <button @click="${this._onClick}" part="button">
        Click count: ${this.count}
      </button>
      <slot></slot>
    `;
  }

  _onClick(){
    this.count++;
  }
}window.customElements.define('counter-component', MyElement)
